<?php
require_once 'lib/nusoap.php';
require 'vendor/autoload.php';
require_once('config.php');
require_once('connect.php');
require_once('cone.php');
require_once('NumeroALetras.php');
use Spipu\Html2Pdf\Html2Pdf;
use Dompdf\Dompdf;
use Dompdf\Options;


$config = new Config();

$conectMYSQL = $connMysql;
$conectMSSQL = $conn;

foreach ($_POST['data'] as $key => $value) {

        
    if (array_key_exists('id', $value)) {

        Certification($conectMYSQL,$conectMSSQL, $value['solicitud'], $value['institucion'], $key, $value['actividad'], $_POST['entidad'],$config );
    }
    
    

}
echo '<script> window.history.back();</script>';





//metodos

function Certification($connMysql, $conn, $no_solicitud, $institucion, $id, $actividad, $entidad,$config )
{      
    
    $fileReader = fopen("log.txt","r");
    $text = '';
    while(!feof($fileReader)){
        $text .= fgets($fileReader);
    }

    $file = fopen("log.txt","w");
        
        $sql = "SELECT * FROM documentos  WHERE  id=$id";
        $result = $connMysql->query($sql);
        $row = $result->fetch_assoc();


        $html = $row['html'];
        $categoria = $row['categoria'];
        $title = 'CERTIFICACION DE '.utf8_encode ($row['nombre']).' '.$id;
     
      
        $hora = getdate()['hours'].":".getdate()['minutes'].":".getdate()['seconds'];
        $fecha = getdate()['mday']."/".getdate()['mon']."/".getdate()['year'];
        fwrite($file, "--------Bienvenido! [".$fecha.": ".$hora."][".$institucion."]--------" . PHP_EOL);
        fwrite($file, $hora.": Creando el Documento" . PHP_EOL);     
        $ejecutarAtrib = sqlsrv_query($conn, "SELECT * FROM dyn".$entidad." WHERE A1NUMSOLICITUD='".$no_solicitud."'");
        $filaAtrib = sqlsrv_fetch_array($ejecutarAtrib, SQLSRV_FETCH_ASSOC);

        if($row['iddoc']){
            $delete = EraseDocument($categoria, $row['iddoc'],$config );
            fwrite($file, $hora.": Message de EraseDocument() ".$delete . PHP_EOL); 
            fwrite($file, $hora.": El documento ".$filaCategory['Exist'].' fue eliminado' . PHP_EOL);
        }
       
        
            
            $atributte = "idservicio=$no_solicitud;EstadoFinalDoc=Disponible Ciudadano;";
            
            foreach ($filaAtrib as $key => $value) {
                //si hay un valor porcentaje en MONTO lo formatea a [00.00]
                if(substr($key, 0, -2) == 'MONTO'){
                    if($value){
                        fwrite($file, $hora.": Se encontro valores decimales y se formatearon a [00.00], line 137" . PHP_EOL);
                         $float = explode(".", $value);
                         $value = $float['0'] . '.'.substr($float['1'], 0, 2);
                    }
                }
                
                //si es hora convertir a formato HH:MM
                $hora = strpos($key, 'HORA');
                if ($hora !== false) {
                    
                    if (strlen($value) == 5){
                        $value = '0'.$value;
                    }
                    $value = substr($value, 0, 2).':'.substr($value, 2, 2);
                } 
                //si es dateTime convertir a string
                if(is_object($value)){
                    fwrite($file, $hora.": Se encontraron valores tipo fecha y se formatearon a [00/00/0000], line 146" . PHP_EOL);
                    $pos = strpos($html, '#!'.$key.'#');
                    if ($pos !== false) {
                        
                        $value = DateTo($value);
                        $resultado = str_replace('#!'.$key.'#',  $value, $html);
                    } else{
                        $pos = strpos($html, '#!!'.$key.'#');
                        if($pos !== false){
                            $value = mb_strtolower(DateTo($value));
                            $resultado = str_replace('#!!'.$key.'#',  $value
                            , $html);
                        }else{
                            $value = $value->format('d-m-Y');
                            $resultado = str_replace('#'.$key.'#',  utf8_encode($value), $html);
                        }
                        
                    }
                   
                    
                    
                }else{
                  
                $resultado = str_replace('#'.$key.'#',  utf8_encode($value), $html);
                }
                
               
                
                $html = $resultado;
                
                
            }
            
               
                
                //crea un nuevo documento y asginarle atributos
                $idCertificacion = newDocument($categoria, $title, $no_solicitud, $atributte,$config );
                fwrite($file, $hora.": Se creo el Documento[".$idCertificacion."], line: 167" . PHP_EOL);

                if(!$row['iddoc']){
                    $queryForIdDoc = "UPDATE documentos SET iddoc='$idCertificacion' WHERE id=$id ";
                    if ($connMysql->query($queryForIdDoc) === TRUE) {
                    fwrite($file, $hora.": se guardo el id del documento en la base de datos". PHP_EOL); 
                        $connMysql->close();
                    } 
            
                }   
            






            
                // generar el pdf
                //subir archivo electronico
                /*
                $resultado = str_replace('src="',  'src="'.$_SERVER["SERVER_NAME"], $html);
                print_r($resultado);
                exit;
                */
               
                $pdf = SaveCertification($idCertificacion,$title,$html,$config );
                fwrite($file, $hora.": ".$pdf['detail'].", line: 175" . PHP_EOL);
                
                $ruta = $pdf['ruta'];
                
                if($pdf['detail'] == '1: Operación realizada con éxito'){
                    echo '
                    <script>
                        
                    window.open("'.$ruta.'", "_blank");
                    window.open("./deletePdf.php?ruta='.$ruta.'", "_blank");
                    </script>';
                }else {
                    
                    echo '<center><h2vstyle="color: red">Hubo un problema al crear la certificacion contacte a erickson  juyendo! o.o!!</h2></center>';
                    echo '<center><h3>500 internal server error</h3></center>';
                }
    
                //asocialo
                $asoc =  AsociateDocument($idCertificacion,$no_solicitud, $actividad,$config);
                fwrite($file, $hora.": Asociando => ".$asoc['Detail']. " con id ".$idCertificacion.", line: 171" . PHP_EOL);
                fwrite($file, $hora.": Termino el  proceso de las certificaciones". PHP_EOL);  
                fclose($file);
                
                
}



//metodos




function eliminar_tildes($cadena){
 
    //Codificamos la cadena en formato utf8 en caso de que nos de errores
    //$cadena = utf8_encode($cadena);
 
    //Ahora reemplazamos las letras
    $cadena = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $cadena
    );
 
    $cadena = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $cadena );
 
    $cadena = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $cadena );
 
    $cadena = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $cadena );
 
    $cadena = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $cadena );
 
    $cadena = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C'),
        $cadena
    );
 
    return $cadena;
}

function DateTo($date){
    //$data = explode("-", $date);
   
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $dia = 'a los '.ucwords(strtolower(NumeroALetras::convertir($date->format('d')))).'('.$date->format('d').') días';
    $mes = $meses[$date->format('m')-1];
    $ano = ucwords(strtolower(NumeroALetras::convertir($date->format('Y')))).'('.$date->format('Y').')';
    $arrayDate = array('dia' => $dia, 'mes' => $mes, 'ano' => $ano);
    
    $dateFinal = $arrayDate['dia'].' de '.$arrayDate['mes'].' del año '.$arrayDate['ano'];
    
    return $dateFinal;
}



function WriteDate($date){
    setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
    $WriteDate = strftime("%d de %B de %Y", strtotime($date));
    return $WriteDate;
}

function newDocument($category, $title, $no_solicitud, $atrib,$config )
{

   
    

    $dc_ws = new nusoap_client('https://'.$config::HOSTSE.'/softexpert/webserviceproxy/se/ws/dc_ws.php', false);

    $dc_ws->setCredentials($config::USERWS,$config::PASSWS);
    $params = array('idcategory' => $category,'IDDOCUMENT'=>'','title' =>'CERTIFICACION DE '. $title,'DSRESUME'=>'', 'DTDOCUMENT' =>'', 'ATTRIBUTES' => $atrib ,'IDUSER' =>'splws01');
    $request = $dc_ws ->call('newDocument',$params);
   
    $separado = explode(": ", $request);
    $DocumentID = $separado[1];
  
    return $DocumentID;
}


function AsociateDocument($iddoc,$WorkflowID, $actividad,$config )
{
    $dc_custom = new nusoap_client('https://'.$config::HOSTSE.'/softexpert/webserviceproxy/se/ws/wf_ws.php');

        
        $params = array('workflowID'=>$WorkflowID,'activityID'=>$actividad,'documentID'=>$iddoc);
        print_r($params );
        $dc_custom->setCredentials($config::USERWS,$config::PASSWS);
        $request = $dc_custom->call('newAssocDocument',$params);
     
     return $request;
    
    
}

function EraseDocument($category, $document,$config )
{

   $dc_ws = new nusoap_client('https://'.$config::HOSTSE.'/softexpert/webserviceproxy/se/ws/dc_ws.php',false);

   $dc_ws->setCredentials($config::USERWS,$config::PASSWS);
   $params = array('idcategory' =>$category,'iddocument'=>$document,'IDUSER'=>'40224190500','justify' =>'asd');

   $request = $dc_ws ->call('deleteDocument',$params);
    return $request;
}

function html5pdf($id_doc,$title,$html){
    ob_start();
  
    $content =  formatHtml($html);
    $nombre = str_replace(' ','_',$title);
    
    $ruta = 'Certificaciones/'.eliminar_tildes($nombre).'lalalalalla.pdf';
    $html2pdf = new Html2Pdf();
    $html2pdf->writeHTML($content);
    ob_get_clean();

    ob_end_clean();
    $output = $html2pdf->output();
    file_put_contents($ruta, $output);
    
    if (file_exists($ruta)) {
        $detail = utf8_encode(UploadFile($id_doc,$ruta,$title));
    } else {
        $detail = 'error al subir la certificacion';
    }

    return array('ruta' => $ruta, 'detail' => $detail);
}

function SaveCertification($id_doc,$title,$html,$config ){
    $content =  formatHtml($html);
    $nombre = str_replace(' ','_',$title);
    
    
    $ruta = 'Certificaciones/'.eliminar_tildes($nombre).'.pdf';
    // instantiate and use the dompdf class
    
    $dompdf = new Dompdf();
    $dompdf->set_option('isHtml5ParserEnabled', true);
    $options = new Options();
    $options->setIsRemoteEnabled(true);
    $dompdf->setOptions($options);
    $dompdf->loadHtml($content);
    $contxt = stream_context_create([ 
        'ssl' => [ 
            'verify_peer' => FALSE, 
            'verify_peer_name' => FALSE,
            'allow_self_signed'=> TRUE
        ] 
    ]);
    $dompdf->setHttpContext($contxt);
   
    
    // (Optional) Setup the paper size and orientation
    //$dompdf->setPaper('A4', 'portrait');

    // Render the HTML as PDF
    $dompdf->render();

    // Output the generated PDF to Browser
    $output = $dompdf->output();
    file_put_contents($ruta, $output);

    if (file_exists($ruta)) {
        $detail = utf8_encode(UploadFile($id_doc,$ruta,$title,$config ));
    } else {
        $detail = 'error al subir la certificacion';
    }

    return array('ruta' => $ruta, 'detail' => $detail);
    
}


function OnlyPdf($id_doc,$title,$html){
    $content =  formatHtml($html);
    $nombre = str_replace(' ','_',$title);
    $ruta = 'Certificaciones/'.$nombre.'.pdf';
    // instantiate and use the dompdf class
       
    $dompdf = new Dompdf();
   
    $options = new Options();
    $options->setIsRemoteEnabled(true);

    $dompdf->setOptions($options);
  
    $dompdf->loadHtml($content);

    // (Optional) Setup the paper size and orientation
    //$dompdf->setPaper('A4', 'portrait');

    // Render the HTML as PDF
    $dompdf->render();

    // Output the generated PDF to Browser
    $output = $dompdf->output();
    file_put_contents($ruta, $output);

   
        $detail = 'Solo se genero el Documento';
    

    return array('ruta' => $ruta, 'detail' => $detail);

}


function UploadFile($doc,$ruta,$title,$config ){
   

    $client = new nusoap_client('https://'.$config::HOSTSE.'/softexpert/webserviceproxy/se/ws/dc_ws.php',false);

    $client->setCredentials($config::USERWS,$config::PASSWS);
    $nombre = str_replace(' ','_',$title);
   
    $b64Doc = chunk_split(base64_encode(file_get_contents($ruta))); 
    
    $items = array('NMFILE'=>eliminar_tildes($nombre).'.pdf','BINFILE'=> $b64Doc, 'ERROR'=>'');
    $file = array('item'=>$items);
    $params = array('IDDOCUMENT' => $doc,'IDREVISION'=>'00','IDUSER' =>'splws01','FILE'=>$file);
    
    $response = $client->call('uploadEletronicFile',$params);
    
    //print_r($client);


    return $response ;

}



function formatHtml($html)
{   $value = '';
    $style = "<style> .line{line-height: 0.2;} html { margin-bottom: 0; margin-top: 10;}</style>";
    $html = $style.$html;
    $data = explode("</p>", $html);
    for ($i=0; $i <count($data) ; $i++) { 
        if(strlen($data[$i]) < 100){
            $data[$i] = str_replace('<p ', '<p class="line"', $data[$i]);
            $data[$i] = str_replace('<p>', '<p class="line">', $data[$i]);
            
        
        }
        

        $value .= $data[$i];
    }
    //print_r($value);
    //exit;
    return $value;
    
}

/*
if(strpos($value, '.')  === true){
                        $decimal = explode(".", $value);
                        $value = $decimal[0] + substr($decimal[1],0, 2);
                        print_r($value);
                        exit;
                    }
*/ 


?>