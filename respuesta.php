<?php

$institucion = $_GET['institucion'];
$proceso = $_GET['proceso'];
$categoria = $_GET['categoria'];
$entidad = $_GET['entidad'];


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Archivo subido Correctamente</title>
    <!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.9/css/mdb.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <style>
        .container{
            padding: 5%
        }
    </style>
</head>
<body>
    <div class="container">
       <center>
            <img class="animated  bounceInDown delay-2s" src="https://www.semanadamulherbemresolvida.com.br/wp-content/uploads/2018/02/checkmark.gif" alt="correcto">
            <br/><br/>
            <h2 class="animated  bounceInUp delay-2s">el modelo <?php echo $institucion?> fue creado correctamente!</h2>
            
            <a href="newCertf.php?institucion=<?php echo $institucion?>&entidad=<?php echo $entidad?>" style="background-color: #78B347"class="animated white-text  bounceInUp delay-2s btn  btn-lg">Crear nueva Certificacion <?php echo $institucion?></a>
       </center>
    </div>

    <!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.9/js/mdb.min.js"></script>
</body>
</html>