<?php
require_once('connect.php');
require_once('cone.php');

$proceso = $_GET['proceso'] ;
$no_solicitud = $_GET['solicitud'];
$institucion = $_GET['institucion'];
$actividad = $_GET['activity'];
$entidad = $_GET['entidad'];




$ejecutar = sqlsrv_query($conn, "SELECT * FROM dyn".$entidad." WHERE A1NUMSOLICITUD='".$no_solicitud."'");
      //print_r("SELECT * FROM dyn".$entidad." WHERE A1NUMSOLICITUD='".$no_solicitud."'");
$filaServer = sqlsrv_fetch_array($ejecutar, SQLSRV_FETCH_ASSOC);

if($filaServer['TIPOCERTI']){
    $nombreCert = $filaServer['TIPOCERTI'];
    $sql = "SELECT * FROM documentos  WHERE  proceso = '$proceso' AND  categoria in($nombreCert)";
    
    
} else {
    $sql = "SELECT * FROM documentos  WHERE  proceso = '$proceso'";
}
//print_r($sql);
$result = $connMysql->query($sql);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html;" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Certificaciones</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.9/css/mdb.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<style>
    .check {
        width:20px;
        height:20px;
        
    }
</style>
</head>
<body>

<br>
        
        <div class="row  animated  bounceInDown delay-2s">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="card blue darken-4 text-center z-depth-4" style="border: 2px solid white; border-radius: 80px;" >
                <div class="card-body">
                <h4 class="text-center white-text">TODOS LOS MODELOS DE <?php echo $institucion?></h4>
               
                </div>
            </div>
        </div>
       
        <div class="col-md-4"></div>
        </div>

    <form action="GenerateCard.php"  onsubmit="loading();" method="post">
        
        <div class="row animated  bounceInUp delay-2s" style="padding: 15px;">
            <?php
            
            while($row = $result->fetch_assoc()) {
              
            ?>
                <div style="padding: 25px;"  class="col-md-3">

                        <input type="hidden" name="data[<?php echo $row['id']?>][categoria]" value="<?php echo $row['categoria']?>"/>
                        <input type="hidden" name="data[<?php echo $row['id']?>][solicitud]" value="<?php echo $no_solicitud?>"/>
                        <input type="hidden" name="data[<?php echo $row['id']?>][institucion]" value="<?php echo $institucion?>"/>
                        <input type="hidden" name="data[<?php echo $row['id']?>][actividad]" value="<?php echo $actividad?>"/>
                        <input type="hidden" name="entidad" value="<?php echo $entidad?>"/>
                        <input type="checkbox" style="visibility: hidden;" name="data[<?php echo $row['id']?>][id]" class="check" id="<?php echo $row['id']?>" />

                         <div class="card text-center" id="div<?php echo $row['id']?>"  onclick="check(<?php echo $row['id']?>)">
                            <div class="card-header blue darken-4 white-text">
                                <strong style="margin-left: 40px;"><?php echo $row['categoria']?><i style="visibility: hidden;" id="icon<?php echo $row['id']?>" class="fa fa-check green-text fa-2x" aria-hidden="true"></i></strong> 
                            </div>
                            <div class="card-body">
                                <h6 style="font-size: 13px" class="card-title"><?php echo utf8_encode($row['nmcategoria'])?></h6>
                                    
                                <a href="editCertif.php?id=<?php echo $row['id']?>&solicitud=<?php echo $no_solicitud?>&institucion=<?php echo $institucion?>&entidad=<?php echo $entidad?>&activity=<?php echo $actividad?>" class="btn  red accent-4 white-text btn-sm">Editar</a>
                            </div>
                            <div class="card-footer text-muted blue darken-4 white-text">
                                <p style="font-size: 13px" class="mb-0"><?php  echo utf8_encode($row['nombre'])?></p>
                            </div>
                        </div>

                </div> 
                

                
                


            <?php
            
            }
            ?>
        
        </div>
        <div class="row" style="margin-top: 30px;">
        
            <div class="col-md-12">
                <center><input type="submit" value="Generar Documentos"    class=" btn red accent-4 white-text waves-effect btn-lg "/></center>
            </div>
        
        </form>
        
<!-- Modal -->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
      <center><img src="./load.svg" width="150" alt=""></center><br>
      <h5 class="text-center">Generando documentos ...</h5>
      <p class="text-center">Este proceso tardara varios segundos</p>
      </div>
      
    </div>
  </div>
</div>
        
</body>
   <!-- JQuery -->
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.9/js/mdb.min.js"></script>
<script>
    var windowName = 'userConsole';
    var popUp = window.open('/AppsOPTIC/APP_Certificaciones/createcertification/deletePdf.php', windowName, 'width=1000, height=700, left=24, top=24, scrollbars, resizable');
    if (popUp == null || typeof(popUp)=='undefined') {  
        alert('Por favor deshabilita el bloqueador de ventanas emergentes y vuelve a hacer clic".');
    }
    
    function check(id){
        var div = document.getElementById('div'+id);
        var icon = document.getElementById('icon'+id);
        var value = document.getElementById(id).checked;
        document.getElementById(id).checked = !value;

        if(value){
            div.className = " card text-center animated  bounce delay-1s"
            icon.style.visibility = "hidden";

        }else{
            
            div.className = " card text-center animated  bounceIn delay-2s"
            icon.style.visibility = "visible";
        }
    }

    function loading() {
        $('#basicExampleModal').modal('show')
    }
</script>
</html>