<?php

/**
 * Clase que implementa un coversor de números
 * a letras.
 *
 * Soporte para PHP >= 5.4
 * Para soportar PHP 5.3, declare los arreglos
 * con la función array.
 *
 * @author AxiaCore S.A.S
 *
 */
class NumeroALetras
{
    private static $UNIDADES = [
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
    ];
    private static $DECENAS = [
        'VENTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
    ];
    private static $CENTENAS = [
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
    ];
    public static function convertir($number, $moneda = '', $centimos = '', $forzarCentimos = false)
    {
        $converted = '';
        $decimales = '';
        if (($number < 0) || ($number > 999999999)) {
            return 'No es posible convertir el numero a letras';
        }
        $div_decimales = explode('.',$number);
        if(count($div_decimales) > 1){
            $number = $div_decimales[0];
            $decNumberStr = (string) $div_decimales[1];
            if(strlen($decNumberStr) == 2){
                $decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
                $decCientos = substr($decNumberStrFill, 6);
                $decimales = self::convertGroup($decCientos);
            }
        }
        else if (count($div_decimales) == 1 && $forzarCentimos){
            $decimales = 'CERO ';
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', self::convertGroup($millones));
            }
        }
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', self::convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', self::convertGroup($cientos));
            }
        }
        if(empty($decimales)){
            $valor_convertido = $converted . strtoupper($moneda);
        } else {
            $valor_convertido = $converted . strtoupper($moneda) . ' CON ' . $decimales . ' ' . strtoupper($centimos);
        }
        return $valor_convertido;
    }
    private static function convertGroup($n)
    {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = self::$CENTENAS[$n[0] - 1];
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= self::$UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }
}

function Dia($dia) {
    $letra = '';
    switch ($dia) {
            case 1:
            $letra = 'Un';
            break;
            case 2:
            $letra = 'Dos';
            break;
            case 3:
            $letra = 'Tres';
            break;
            case 4:
            $letra = 'Cuatros';
            break;
            case 5:
            $letra = 'Cincos';
            break;
            case 6:
            $letra = 'Seis';
            break;
            case 7:
            $letra = 'Siete';
            break;
            case 8:
            $letra = 'Ocho';
            break;
            case 9:
            $letra = 'Nueve';
            break;
            case 10:
            $letra = 'Diez';
            break;
            case 11:
            $letra = 'Once';
            break;
            case 12:
            $letra = 'Doce';
            break;
            case 13:
            $letra = 'Trece';
            break;
            case 14:
            $letra = 'Catorce';
            break;
            case 15:
            $letra = 'Quince';
            break;
            
            case 16:
            $letra = 'Dieciseis';
            break;
            case 17:
            $letra = 'Diecisiete';
            break;
            case 18:
            $letra = 'Dieciocho';
            break;
            case 19:
            $letra = 'Diecinueve';
            break;
            case 20:
            $letra = 'Veinte';
            break;

            case 21:
            $letra = 'Veintiuno';
            break;
            case 22:
            $letra = 'Veintidos';
            break;
            case 23:
            $letra = 'Veintitres';
            break;
            case 24:
            $letra = 'Veinticuatro';
            break;
            case 25:
            $letra = 'Veinticinco';
            break;
            case 26:
            $letra = 'Veintiseis';
            break;
            case 27:
            $letra = 'Veintisiete';
            break;
            case 28:
            $letra = 'Veintiocho';
            break;
            case 29:
            $letra = 'Veintinueve';
            break;
            case 30:
            $letra = 'Treinta';
            break;
            case 31:
            $letra = 'Treinta Y Uno';
            break;
            
        
        default:
            # code...
            break;

            return 'a los '.$letra.'('.$dia.') dias';
    }

}


?>