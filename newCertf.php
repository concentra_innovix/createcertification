

<?php

require_once('cone.php');

$institucion = $_GET['institucion'];
$entidad = $_GET['entidad'];



$category = $institucion."DS";

$query = "SELECT TOP  1 *  FROM dyn".$entidad;



$ejecutar = sqlsrv_query($conn, $query);

$fila = sqlsrv_fetch_array($ejecutar, SQLSRV_FETCH_ASSOC);

$queryCategory = "SELECT IDCATEGORY 'id', NMCATEGORY 'Nombre' FROM DCCATEGORY WHERE IDCATEGORY LIKE '%$category%'";

$ejecutarCategory = sqlsrv_query($conn, $queryCategory);



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.9/css/mdb.min.css" rel="stylesheet">
    <style>
        #buscador{
            background-color: #E6E6E6;
           height: 400px;
            overflow: auto;
        }
        #myInput {
    background-image: url('/css/searchicon.png'); /* Add a search icon to input */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
}

#lista {
    /* Remove default list styling */
    list-style-type: none;
    padding: 0;
    margin: 0;
}

#lista li a {
    border: 1px solid #ddd; /* Add a border to all links */
    margin-top: -1px; /* Prevent double borders */
    border-radius: 25px;
    background-color: white; /* Grey background color */
    padding: 12px; /* Add some padding */
    text-decoration: none; /* Remove default text underline */
    font-size: 18px; /* Increase the font-size */
    color: black; /* Add a black text color */
    display: block; /* Make it into a block element to fill the whole list */
}

#lista li a:hover:not(.header) {
    background-color: white; /* Add a hover effect to all links, except for headers */
}

    .border{
        border: 100px solid ;
        border-radius: 25px;
        border-color: red;
        background-color: #0d47a1;
    }
    
    </style>
</head>
<body>
    
    
    
    <div  style="padding-left: 30px; padding-right: 30px; padding-top: 10px">
    <form id="form" onsubmit="return confirm('esta seguro que los atributos que asigno al documento estan en la categoria <?php echo $category?>?');" action="saveCertif.php" method="post">
        <div class="row border" >
            
            <div class="col-md-9" style="padding: 10px;">
            
               
                        
                     
                    <textarea  name="content" id="content" rows="20" cols="20">
                    
                    </textarea>
                    
                    
                
            </div>
            
            <div class="col-md-3"  style="padding: 10px; background-color: white;   border-right: 3px solid #0d47a1; border-top: 3px solid #0d47a1; border-bottom: 3px solid #0d47a1;  border-radius: 25px;">
                    
                   
                        <select name="category" class="form-control">
                        <?php
                            while($filaCategory = sqlsrv_fetch_array($ejecutarCategory)){
                               
                                
                        ?>
                            <option value="<?php echo $filaCategory['id'] ?>,<?php echo utf8_encode($filaCategory['Nombre']) ?>"><?php echo utf8_encode($filaCategory['Nombre']) ?></option>
                        <?php
                        }
                        ?> 
                    </select><br>

                <input type="hidden" name="institucion" class="form-control" value="<?php echo $institucion?>"/>
                <input type="text" name="nombre" class="form-control"placeholder="Nombre del documento" /><br>
                <input type="text" name="proceso" class="form-control"placeholder="ID del proceso" />
                <input type="hidden" name="entidad" class="form-control" value="<?php echo $entidad?>" />
                <hr>
                <input type="text"id="myInput" onkeyup="buscar()"  class="form-control" placeholder="buscar variable.."/>
                <div id="buscador">
                <ul id="myUL" >
                <?php
                    
                    foreach ($fila as $key => $value) {
                       
                        if(is_object($value)){
                            $value = $value->format('d-m-Y');
                            
                        }
                        
                        
                
                ?>

                   
                    <li><a><b>#<?php echo $key?># </b></a><?php echo ''?><hr></li>
                <?php
                        
                    }
                ?>
                </ul>
                
                </div>
                <br>
                <center><button class="btn white-text red accent-4 btn-block">Guardar</button></center>
            </div>
        </div>
        </form>
    </div>
    
        <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.9/js/mdb.min.js"></script>
    <script src="../createcertification/ckeditor/ckeditor.js"></script>
    <script>
       
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                
                    
                CKEDITOR.replace( 'content'); 
                    
                
                //CKEDITOR.config.line_height="0;0.5;1" 
                function SelectAll(id)
                {
                    document.getElementById(id).focus();
                    document.getElementById(id).select();
                }
                        function buscar() {
                            
                            // Declare variables
                            var input, filter, ul, li, a, i;
                            input = document.getElementById('myInput');
                            filter = input.value.toUpperCase();
                            ul = document.getElementById("myUL");
                            li = ul.getElementsByTagName('li');

                            // Loop through all list items, and hide those who don't match the search query
                            for (i = 0; i < li.length; i++) {
                                
                                if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                                    li[i].style.display = "";
                                } else {
                                    li[i].style.display = "none";
                                }
                            }
                        }

                        

                         
    </script>
</body>
</html>